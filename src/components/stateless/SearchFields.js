import React from "react";
import { Select, Button } from "antd";
import { Row, Col } from "antd";

const Option = Select.Option;

const SearchFields = props => {
  return (
    <div>
      <Row type="flex" justify="center">
        <Col>
          <Select
            style={{ width: 180, padding: 10 }}
            onChange={val => props.handleDropDownSelect(val, "_EXPERIENCE")}
            placeholder="Select Experience"
          >
            {renderExperience()}
          </Select>
        </Col>
        <Col>
          <Select
            style={{ width: 180, padding: 10 }}
            placeholder="Select Location"
            onChange={val => props.handleDropDownSelect(val, "_LOCATION")}
          >
            {renderLocation()}
          </Select>
        </Col>
        <Col>
          <Select
            style={{ width: 180, padding: 10 }}
            placeholder="Select Skills"
            onChange={val => props.handleDropDownSelect(val, "_SKILLS")}
          >
            {renderSkills()}
          </Select>
        </Col>
      </Row>
      <Row type="flex" justify="center">
        <Col>
          <Button
            style={{ marginTop: 20 }}
            type="primary"
            shape="circle"
            icon="search"
            onClick={props.handleClick}
          />
        </Col>
      </Row>
    </div>
  );
};

const renderExperience = () => {
  let optsArray = [];
  for (var i = 1; i < 16; i++) {
    optsArray.push(<Option key={i} value={i}>{i}</Option>);
  }
  optsArray.unshift(<Option  key='Fresher' value="Fresher">Fresher</Option>);
  optsArray.unshift(<Option key='All' value="All">All</Option>);
  return optsArray;
};

const renderLocation = () => {
  let LocationArray = [
    "All",
    "Bengaluru",
    "Hyderabad",
    "NCR",
    "Cochin",
    "Any",
    "New Delhi",
    "Mumbai"
  ];
  return LocationArray.map(city => <Option key={city} value={city}>{city}</Option>);
};

const renderSkills = () => {
  let SkillsArray = [
    "All",
    "Frontend",
    "Backend",
    "HR",
    "Digital marketing",
    "Social Media",
    "Content Writer"
  ];
  return SkillsArray.map(skills => <Option key={skills} value={skills}>{skills}</Option>);
};

export default SearchFields;
