import React, { Component } from "react";
import SearchFields from "../../stateless/SearchFields";
import JobListing from "./JobListing";
import { JobFilterBar, JobResults } from "./JobListing";
import { Row, Spin } from "antd";

class HomePageWrapper extends Component {
  state = {
    _EXPERIENCE: "",
    _LOCATION: "",
    _SKILLS: "",
    _COMPANYNAME: "",
    _EXPERIENCESORT: false,
    _LOCATIONSORT: false
  };
  handleClick = val => {
    this.props.handleSearch(val);
  };

  handleDropDownSelect = (val, type) => {
    this.setState({ [type]: val });
  };

  handleSortByExperience = () => {
    this.setState({ _EXPERIENCESORT: true, _LOCATIONSORT: false });
  };

  handleSortByLocation = () => {
    this.setState({ _LOCATIONSORT: true, _EXPERIENCESORT: false });
  };

  handleFilterByCompany = key => {
    this.setState({ _COMPANYNAME: key.target.value });
  };

  render() {
    return (
      <React.Fragment>
        <SearchFields
          handleClick={this.handleClick}
          handleDropDownSelect={this.handleDropDownSelect}
        />
        {this.props.res === "Loading" ? (
          <Row type="flex" justify="center">
            <Spin style={{ marginTop: 50 }} size="large" />
          </Row>
        ) : (
          <JobListing
            data={this.props.res}
            dropDownFilters={this.state}
            handleSortByExperience={this.handleSortByExperience}
            handleFilterByCompany={this.handleFilterByCompany}
            handleSortByLocation={this.handleSortByLocation}
          >
            <JobFilterBar />
            <JobResults />
          </JobListing>
        )}
      </React.Fragment>
    );
  }
}

export default HomePageWrapper;
