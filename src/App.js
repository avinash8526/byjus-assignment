import React, { Component } from "react";
import "./App.css";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from "./pages/Home";
import AppLayout from "./components/stateful/Layouts/AppLayout";
import { withRouter } from "react-router";

class App extends Component {
  render() {
    return (
      <AppLayout>
        <Switch>
          <Route exact path="/" component={Home} />
          <Redirect to="/" />
        </Switch>
      </AppLayout>
    );
  }
}

export default withRouter(App);
