export const getFilteredDataByExpireTime = data => {
  return data.filter(post => {
    let postEndDate;
    let todayDate = new Date();
    todayDate.setHours(0, 0, 0, 0);
    if (post.enddate.length > 0) {
      postEndDate = new Date(post.enddate).getTime();
      return postEndDate - todayDate > 0;
    }
    return true;
  });
};

export const getFilteredDataByUserCriteria = (data, filterData) => {
  debugger;
  if (!filterData) {
    return data;
  }
  return data
    .filter(post => {
      if (
        filterData["_EXPERIENCE"] === "All" ||
        filterData["_EXPERIENCE"] === ""
      ) {
        return true;
      }
      var re = new RegExp(`${filterData["_EXPERIENCE"]}\\b`, "gi");
      if (post.experience.length === 0 || post.experience === undefined) {
        return true;
      } else {
        let matched = post.experience.match(re);
        return matched !== null;
      }
    })
    .filter(post => {
      if (filterData["_SKILLS"] === "All" || filterData["_SKILLS"] === "") {
        return true;
      }
      var re = new RegExp(`${filterData["_SKILLS"]}\\b`, "gi");
      if (post.skills.length === 0 || post.skills === undefined) {
        return true;
      } else {
        let matched = post.skills.match(re);
        return matched !== null;
      }
    })
    .filter(post => {
      if (filterData["_LOCATION"] === "All" || filterData["_LOCATION"] === "") {
        return true;
      }
      var re = new RegExp(`${filterData["_LOCATION"]}\\b`, "gi");
      if (post.location.length === 0 || post.location === undefined) {
        return false;
      } else {
        let matched = post.location.match(re);
        return matched !== null;
      }
    })
    .filter(post => {
      if (filterData["_COMPANYNAME"] === "") {
        return true;
      }
      debugger;
      var re = new RegExp(`${filterData["_COMPANYNAME"]}`, "gi");
      if (post.companyname.length === 0 || post.companyname === undefined) {
        return false;
      } else {
        let matched = post.companyname.match(re);
        return matched !== null;
      }
    });
};

export const getSortedData = (data, filterData) => {
  debugger;
  let copiedData = Object.assign({}, data);
  if (filterData["_EXPERIENCESORT"]) {
    data.sort((a, b) => {
      if (a.experience.length === 0 && b.experience.length === 0) {
        return 0;
      }
      if (a.experience.length === 0 && b.experience.length > 0) {
        return 1;
      }
      if (b.experience.length === 0 && a.experience.length > 0) {
        return -1;
      }
      if (a.experience.toLowerCase() === "fresher") {
        return -1;
      }
      var re = new RegExp(`\\d`, "g");
      let experienceA = a.experience.match(re);
      let experienceB = b.experience.match(re);
      if (experienceA !== null && experienceB !== null) {
        return experienceA[0] - experienceB[0];
      }
      return 0;
    });
  }
  if (filterData["_LOCATIONSORT"]) {
    data.sort((a, b) => {
      if (a.location.length === 0 && b.location.length === 0) {
        return 0;
      }
      if (a.location.length === 0 && b.location.length > 0) {
        return 1;
      }
      if (b.location.length === 0 && a.location.length > 0) {
        return -1;
      }
      let num =
        a.location.charAt(0).toLowerCase() > b.location.charAt(0).toLowerCase()
          ? 1
          : -1;
      return num;
    });
  }
  return data;
};
