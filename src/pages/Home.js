import React, { Component } from "react";
import searchAPI from "../api/searchApi";
import HomePageWrapper from "../components/stateful/HomePage/HomePageWrapper";

class Home extends Component {
  state = {
    response: null
  };

  handleSearch = value => {
    this.setState({ response: "Loading" });
    searchAPI
      .get()
      .then(response => {
        this.setState({ response: response.data && response.data.jobsfeed });
      })
      .catch(error => {
        this.setState({ response: null });
        console.log(error);
      });
  };
  render() {
    return (
      <HomePageWrapper
        handleSearch={this.handleSearch}
        res={this.state.response}
      />
    );
  }
}

export default Home;
