import axios from "axios";

const instance = axios.create({
  baseURL: "https://api.myjson.com/bins/kez8a",
  timeout: 5000
});

export default instance;
