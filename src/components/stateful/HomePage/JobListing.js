import React, { Component } from "react";
import { List, Card, Icon, Row, Col, Button, Input, Tag } from "antd";
import { Link } from "react-router-dom";
import JobResultsPresentation from "../../stateless/JobResults";
import JobFilterBarPresentation from "../../stateless/JobFIlterBar";

const JobFilterBar = props => {
  return <JobFilterBarPresentation {...props} />;
};
const JobResults = props => {
  return <JobResultsPresentation {...props} />;
};

class JobListing extends Component {
  render() {
    const { children } = this.props;
    const childrenWithProps = React.Children.map(children, child =>
      React.cloneElement(child, {
        data: this.props.data,
        dropDownFilters: this.props.dropDownFilters,
        handleSortByExperience: this.props.handleSortByExperience,
        handleSortByLocation: this.props.handleSortByLocation,
        handleFilterByCompany: this.props.handleFilterByCompany
      })
    );
    return <>{childrenWithProps}</>;
  }
}

export default JobListing;
export { JobFilterBar, JobResults };
