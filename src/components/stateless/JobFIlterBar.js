import React from "react";
import { Row, Col, Button, Input, Tag } from "antd";
import {
  getFilteredDataByExpireTime,
  getFilteredDataByUserCriteria
} from "../../utils/helpers/SortingHelper";

const JobFilterBar = props => {
  if (props.data) {
    let filteredData = getFilteredDataByExpireTime(props.data);
    let filteredDataOnUserCriteria = getFilteredDataByUserCriteria(
      props.data,
      props.dropDownFilters
    );
    return (
      <Row type="flex" justify="center">
        <Col>
          <Tag style={{ marginTop: 10 }} color="purple">
            {props.data.length} Jobs Found
          </Tag>
          <Tag style={{ marginTop: 10 }} color="red">
            {props.data.length - filteredData.length} Jobs Expired
          </Tag>
          <Tag style={{ marginTop: 10 }} color="red">
            {filteredDataOnUserCriteria.length} Matching Jobs Found
          </Tag>
        </Col>
        <Col>
          <Button
            type="primary"
            style={{ width: 180, margin: 10 }}
            onClick={props.handleSortByLocation}
          >
            Sort By Location
          </Button>
        </Col>
        <Col>
          <Button
            style={{ width: 180, margin: 10 }}
            onClick={props.handleSortByExperience}
          >
            {" "}
            Sort By Exp
          </Button>
        </Col>
        <Col>
          <Input
            style={{ width: 180, margin: 10 }}
            placeholder="Filter By Company"
            onChange={props.handleFilterByCompany}
          />
        </Col>
      </Row>
    );
  } else {
    return null;
  }
};

export default JobFilterBar;
