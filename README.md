##App Screenshots

[<img src="https://imgur.com/MRdJ9Ym.png">](https://imgur.com/MRdJ9Ym.png)

## Steps

After cloning the repo, perform following steps, the project is made on node version `v10.15.1` older version may cause problmes.

### `npm install`

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>

### `npm run test:cov`

it generates the coverage report

### `npm run build`

Builds the app for production to the `build` folder.<br>

## App Assumptions

- The job listing card doesn't display the field which is empty is json response ex: `skills:""`will not be displayed in card where as `skills:'React'` will get displayed.
- To start click on `search icon` button which fetches the data and displays the results.
- I am displaying expired jobs as well, but there is a fields on top which shows information on expired jobs.
- Sort by Location sorts city by Alphabet first character ascii, ex:- Allahabad will come first then Bengaluru
- Sort by experience puts freshers first then looks for these patters 0-4 yrs or 0 to 5 Years, sorting is done on first value of range ex: 1-4 years and 2-5 years so here comparison will be made between 1 and 2.
- Also for some nodes the experience is empty hence these nodes are put at last
- For Sorting and Filtering `Regex` is used as there was no clear pattern in json response.

## Assumptions for top 3 fields

- Select Experience displays all the jobs cards in which experience is empty, for example if you select experience as 8. it will display all the cards with experience as 8 years along with other cards in which expereince is empty.
- Select location does a strict match and only displays cards in which location is matched. Matching is greedy here
- Select Skills also works on lines of Select Experience
- Matching job found also count the expired jobs
