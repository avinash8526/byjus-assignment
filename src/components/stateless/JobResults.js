import React from "react";
import { List, Card, Icon, Row } from "antd";
import {
  getFilteredDataByExpireTime,
  getFilteredDataByUserCriteria,
  getSortedData
} from "../../utils/helpers/SortingHelper";

const JobResults = ({ data, dropDownFilters }) => {
  data = data === null ? [] : data;
  let filteredData = getFilteredDataByExpireTime(data);
  let filteredDataOnUserCriteria = getFilteredDataByUserCriteria(
    data,
    dropDownFilters
  );
  let sortedData = getSortedData(filteredDataOnUserCriteria, dropDownFilters);
  return (
    <div
      style={{
        background: "#fff",
        padding: 24,
        minHeight: "80vh",
        marginTop: 20
      }}
    >
      <Row type="flex" justify="center">
        <List
          grid={{ gutter: 16, xs: 1, sm: 2, md: 4, lg: 4, xl: 4, xxl: 3 }}
          style={{ padding: "1%" }}
          dataSource={sortedData}
          renderItem={item => (
            <List.Item>
              <Card
                title={item.title}
                style={{ height: 600 }}
                actions={[
                  <Icon
                    type="export"
                    onClick={() => window.open(item.applylink, "_blank")}
                  />
                ]}
              >
                {Object.keys(item).map(key =>
                  item[key].length > 0 &&
                  key !== "_id" &&
                  key !== "applylink" ? (
                    <p key={item[key]._id}>
                      <strong key={key}>{key.toUpperCase()}</strong>:
                      {item[key].slice(0, 400)}
                    </p>
                  ) : null
                )}
              </Card>
            </List.Item>
          )}
        />
      </Row>
    </div>
  );
};

export default JobResults;
