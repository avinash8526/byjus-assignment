import { Layout } from "antd";
import React, { Component } from "react";
import {  PageHeader } from "antd";
import { Row, Col } from "antd";
const { Footer, Content } = Layout;

class AppLayout extends Component {
  render() {
    return (
      <div>
        <Layout>
          <Row type="flex">
            <Col span={24}>
              <PageHeader title="ByJu'S" subTitle="JuJu Job Portal" />
            </Col>
          </Row>

          <Content style={{ padding: "20px 50px", background: "#fff" }}>
            {this.props.children}
          </Content>
          <Footer style={{ textAlign: "center" }}>
            Created by Avinash Agrawal
          </Footer>
        </Layout>
      </div>
    );
  }
}

export default AppLayout;
